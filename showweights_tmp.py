import sys
import argparse

from winebokbot.utils.jl import JSONLinesReader

def print_html(output, weighted_corpuses, limit_terms=5):
    """
    Print to stdout an HTML with a histogram of url - weights.

    urls_weighted - list of (url, weight)
    urls_weighted_cleaned- list of (url, weight)

    """
    corpus_names, wcorpus = zip(*weighted_corpuses)
    urls = reduce(lambda s1, s2: s1 & s2, [set(d.keys()) for d in wcorpus])

    print >>output, "<html><body>"
    print >>output, '<table border=1 width="100%">'
    print >>output, "<tr>"
    print >>output, "<td>URL</td>"
    for name in corpus_names:
        print >>output, "<td>%s</td>" % name
    print >>output, "</tr>"

    for url in urls:
        print >>output, "<tr>"
        print >>output, '<td><div style="width: 100"><a href="%s">%s</a></div></td>' %(url, url)
        for corpus in wcorpus:
            print >>output, "<td>"
            print >>output, "<table>"
            for term, weight in corpus[url][:limit_terms]:
                print >>output, "<tr>"
                print >>output, '<td><div style="width: 75">%s</div></td>' % term.encode('utf8')
                print >>output, '<td><div style="width: %s%%; background-color:green">%s</div></td>' % (int(round(float(weight)*100, 0)), weight)
                print >>output, "</tr>"
            print >>output,  "</table>"
            print >>output, "</td>"
        print >>output,  "</tr>"
    print >>output, "</table>"
    print >>output, "</body></html>"


def read_csv(file_like_obj):
    pass

def read_jl(file_like_obj):
    urls = {}
    for item in JSONLinesReader(file_like_obj):
        urls[item['url']] = item['weights']
    return urls


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('format', metavar='FORMAT', nargs=1,
            help='input type: options: txt, jl, json', type=str, default='txt')
    parser.add_argument('weightedcorpus', nargs='+', help='input files. weighted corpus',
        type=argparse.FileType('r'))
    parser.add_argument('-o', '--output', nargs='?', default=sys.stdin, 
        type=argparse.FileType('wb'))
    parser.add_argument('-l', '--max-words', nargs='?', default=10, 
        type=int)

    args = parser.parse_args()

    reader = None
    if args.format[0] in ('txt', 'csv'):
        reader = read_csv
    elif args.format[0] in ('jl', 'jsonlines'):
        reader = read_jl
    else:
        pass

    if not reader:
        print >>sys.stderr, "Unknown format: %s" % args.format
        sys.exit(1)

    weighted_corpuses = [(f.name, reader(f)) for f in args.weightedcorpus]
    print_html(args.output, weighted_corpuses, limit_terms=args.max_words)

if __name__ == '__main__':
    main()

"""
Generate TFIDF weights for the terms on the corpus
"""
import sys
import argparse
import json

from collections import defaultdict

from winebokbot.utils.jl import JSONLinesReader, JSONLinesWriter
from winebokbot.ir.weights import termfreq_vector, TFIDFScoring

from winebokbot import settings


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('cmd', metavar='COMMAND', 
            help="""
            Command to execute. Options: 
             idfdb   - generate the idf db and output a JSON file
             weights - generate a weighted corpus
            """)
    parser.add_argument('input', metavar='INPUT CORPUS', nargs=1,
            help='Corpus file in jsonlist format. "url" and "text" keys are required in each json.', type=argparse.FileType('r'))
    parser.add_argument('output', metavar='OUTPUT', nargs=1,
        help='output file that depends on the command executed', type=argparse.FileType('w'))

    args = parser.parse_args()

    if args.cmd not in ("weights", "idfdb"):
        print >>sys.stderr, "'%s' command not available" % args.cmd
        sys.exit(1)

    docs = []

    doc_freq = defaultdict(int)
    N = 0 # cantidad de documentos en el corpus
    with args.input[0] as inp:
        for doc in JSONLinesReader(inp):
            N += 1
            doc_weights = termfreq_vector(doc['text'])
            # document frequency
            for token in doc_weights:
                doc_freq[token] += 1
            del doc['text']
            doc['weights'] = doc_weights
            docs.append(doc)

    if args.cmd == 'weights':
        scorer = TFIDFScoring(N, docfreq=doc_freq,
            smart_schema=settings.SMART_SCHEME_DOC)
        with args.output[0] as out:
            jlw = JSONLinesWriter(out)
            for doc in docs:
                weights = scorer.compute(doc['weights'].items())
                doc['weights'] = sorted(weights.iteritems(), key=lambda x:x[1], 
                        reverse=True)
                jlw.write(doc)
    elif args.cmd == 'idfdb':
        scorer = TFIDFScoring(N, docfreq=doc_freq,
            smart_schema=settings.SMART_SCHEME_PAGES)
        with args.output[0] as idfdb:
            vidf = scorer.idfvector
            idfdb.write(json.dumps(vidf))

if __name__ == "__main__":
    main()

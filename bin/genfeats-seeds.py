#!/usr/bin/env python
import sys
import math
import nltk
import argparse

from collections import defaultdict

from winebokbot.cleaning import TextCleaner
from winebokbot.utils.jl import JSONLinesReader, JSONLinesWriter
from winebokbot.ir.ngrams import compute_ngram
from winebokbot.utils.tokenizer import tokenize

def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--print-bigrams', action='store_true', help='print list of bigrams', default=False)
    parser.add_argument('--print-histogram', action='store_true', help='print an histogram weight/count', default=False)
    parser.add_argument('-L', '--lambda-param', type=float,
            help="lambda paramter used for CMI calculus", default=0.95)
    parser.add_argument('-Z', '--zeta', type=float,
            help="paramter used to keep at most Z possible (attr, val) bigrams", default=0.5)
    parser.add_argument('-O', '--sort-by',
            help="options: weight,{asc,desc}; attribute,{asc,desc}", default="weight,desc")
    parser.add_argument('-S', '--score', type=int,
            help="score cut-off - minimum score to start checking features", default=0.5)
    parser.add_argument('input', metavar='FILE', nargs='?',
        help='input file', type=argparse.FileType('r'), default=sys.stdin)
    parser.add_argument('-o', '--output', metavar='FILE',
        help='output file that depends on the command executed',
        type=argparse.FileType('w'),
        default=sys.stdout)

    args = parser.parse_args()

    cleaner = TextCleaner(method='textify')
    tokenize_func = nltk.tokenize.RegexpTokenizer("([A-Za-z]+|[0-9]+|%)").tokenize

    bigrams = []
    wordcount = defaultdict(int)
    bigram_count = defaultdict(int)
    nelems = 0
    for item in JSONLinesReader(args.input):
        if item['score'] <= args.score:
            continue
        text = cleaner.clean(item['page_content'].encode('utf8'))
        tokens = []
        for token in tokenize(text, tokenize_func=tokenize_func):
            term = "##NUMBER##" if token.isdigit() else ("##MEASURE##" if token.lower() in ('l', 'litre') else token)
            tokens.append(term)
            nelems += 1
            wordcount[term] += 1

        for bigram in compute_ngram(tokens):
            bigrams.append(bigram)
            bigram_count[bigram] += 1

    bigram_condprob = defaultdict(float)
    # P(x=pattr) = {# of times pattr is in universe}/nelems
    N = float(nelems)
    for pval, pattr in set(bigrams):
        bigram_condprob[(pval, pattr)] += (bigram_count[(pval, pattr)] / N) / (wordcount[pattr] / N)

    cmi_pairs = {}
    for (pval, pattr), weight in bigram_condprob.iteritems():
        if pattr not in cmi_pairs:
            cmi_pairs[pattr] = set()
        cmi_pairs[pattr].add((pval, weight))

    LAMBDA = args.lambda_param
    cmi = {}
    result = {}
    for attr, vals in cmi_pairs.iteritems():
        nvals = []
        z = 0
        for val, w in sorted(vals, key=lambda x:x[1], reverse=True):
            if val == attr:
                continue
            nvals.append(val)
            z += w
            if z > args.zeta:
                break
        if not nvals:
            continue
        result[attr] = nvals
        pn = sum([bigram_count.get((attr, val), 0)/N for val in nvals])
        pd = LAMBDA * sum(wordcount[val]/N for val in nvals) * abs(LAMBDA - 1) * wordcount[attr]/N
        p = pn / pd
        if p != 0.0:
            cmi[attr] = math.log(p)

    ## - print bigrams
    if args.print_bigrams:
        for attr, weight in sorted(cmi.iteritems(), key=lambda x:x[0]):
            print "%s | %.2f | %s" % (attr.ljust(30), weight, list(result[attr]))
    elif args.print_histogram:
        hist = defaultdict(int)
        for attr, weight in sorted(cmi.iteritems(), key=lambda x:x[1], reverse=True):
            hist[int(round(weight,0))] += len(result[attr])
        print "weight,count"
        emax = max(hist.values())
        for w, c in hist.iteritems():
            print "%s | %s" % (str(w).ljust(3), "*" * int(round(float(c)/emax*100,0)))
    else:
        attrs = args.sort_by.split(",")
        field, order = attrs if len(attrs) > 1 else attrs[0], "asc"
        if order not in ("asc", "desc"):
            parser.error("--sort-by option is on the form : field,{desc,asc}")

        keyfunc = (lambda x: x[1]) if field == "weight" else (lambda x: x[0])
        reverse = True if order == "desc" else False

        with args.output as out:
            writer = JSONLinesWriter(out)
            writer.write_iter({attr: {'weight': round(weight, 2), 'values': result[attr]}}
                    for attr, weight in sorted(cmi.iteritems(), key=keyfunc, reverse=reverse))
if __name__ == '__main__':
    main()

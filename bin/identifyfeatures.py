"""
Identify features on the set of documents.
"""
#!/usr/bin/env python
import sys
import nltk
import argparse

from collections import defaultdict

from winebokbot.cleaning import TextCleaner
from winebokbot.utils.jl import JSONLinesReader, JSONLinesWriter
from winebokbot.utils.tokenizer import tokenize


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input', metavar='CORPUS', nargs=1,
            help='Corpus file in jsonlist format. "url" and "text" keys are required in each json.', type=argparse.FileType('r'))
    parser.add_argument('features', metavar='FEATURES', nargs=1,
            help='Corpus file in jsonlist format. "url" and "text" keys are required in each json.', type=argparse.FileType('r'))
    parser.add_argument('-o', '--output', metavar='OUTPUT', 
        help='output file that depends on the command executed', type=argparse.FileType('w'),
        default=sys.stdout)

    args = parser.parse_args()
    cleaner = TextCleaner(method='textify')
    tokenize_func = nltk.tokenize.RegexpTokenizer("([A-Za-z]+|[0-9]+|%)").tokenize

    features = dict((item.keys()[0], item[item.keys()[0]]['values'])
                    for item in JSONLinesReader(args.features[0]))

    months = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"]
    grape_types = features.pop("grape_type")

    with args.output as out:
        jlwrt = JSONLinesWriter(out)
        for item in JSONLinesReader(args.input[0]):
            text = cleaner.clean(item['page_content'].encode('utf8'))
            tokens = []
            for token in tokenize(text, tokenize_func=tokenize_func):
                term = "##NUMBER##" if token.isdigit() else ("##MEASURE##" if token.lower() in ('l', 'litre') else token)
                tokens.append(term)

            docfeats = defaultdict(list)

            doc_grape_types = set(grape_types) & set(tokens)
            if doc_grape_types:
                docfeats["grape_types"] = list(doc_grape_types)

            for feature in features:
                if feature not in tokens:
                    continue
                val = tokens[tokens.index(feature) - 1]
                if val in features[feature]:
                    if feature in months:
                        docfeats["months"].append(feature)
                    else:
                        docfeats[feature].append(val)

            item['features'] = dict(docfeats)
            jlwrt.write(item)


if __name__ == "__main__":
    main()

"""
Script to parse BootCaT corpust.txt file and generate a jsonlines[1]
file.
1. http://help.scrapinghub.com/api.html#jsonlines-format
"""
import sys
import re
import argparse
from winebokbot.utils.jl import JSONLinesWriter

START_LINE = re.compile("^CURRENT URL (.*?)$")

def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input', metavar='FILE', nargs=1,
        help='url of the site being checked', type=argparse.FileType('r'))
    parser.add_argument('-o', '--output', metavar='FILE',
        help='output file', default=sys.stdout, type=argparse.FileType('w'))

    args = parser.parse_args()
    text = []
    doc = {}
    docid = 0
    with args.output as out:
        jlw = JSONLinesWriter(out)
        for line in args.input[0]:
            start = START_LINE.search(line)
            if start:
                if text:
                    # escribe el text del documento anterior
                    doc['text'] = "".join(text)
                    text = []
                    jlw.write(doc)
                doc = {'url': start.group(1), 'docid': docid}
                docid += 1
            else:
                text.append(line)
        doc['text'] = "".join(text)
        jlw.write(doc)

if __name__ == "__main__":
    main()

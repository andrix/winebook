import sys
import argparse

from winebokbot.utils.jl import JSONLinesReader

def print_html(urls_weighted):
    """
    Print to stdout an HTML with a histogram of url - weights.

    urls_weighted - list of (url, weight)

    """
    print "<html><body>"
    print '<table border=1 width="100%">'
    for url, weight in urls_weighted:
        print "<tr>"
        print '<td><div style="width: %s%%; background-color:green"><a href="%s">%s ...</a></div></td>' % (int(round(float(weight)*100, 0)), url, url[:15])
        print "<td>%s</td>" % weight
        print "</tr>"

    print "</table>"

    ranges = {
        (0.0, 0.2): 0,
        (0.2, 0.4): 0,
        (0.4, 0.5): 0,
        (0.5, 0.6): 0,
        (0.6, 0.8): 0,
        (0.8, 1.0): 0,
    }
    for _, weight in urls_weighted:
        for a, b in ranges.keys():
            if a <= weight < b:
                ranges[(a, b)] += 1
                break


    total = sum(ranges.values())
    print "Total = %s<br>" % total

    print '<table border=1 width="100%">'
    for _range, cant in sorted(ranges.items()):
        print "<tr>"
        print '<td style="width: 10%%">%s</td>' % str(_range)
        print '<td style="width: 90%%"><div style="width: %s%%; background-color:green">.</div></td>' % int(round(float(cant)/total * 100, 0))
        print '<td style="width: 10%%">%d</td>' % cant
        print "</tr>"
    print "</table>"

    print "</body></html>"


def read_jl(file_like_obj):
    urls = []
    try:
        for item in JSONLinesReader(file_like_obj):
            urls.append((item['url'], item['score']))
    except:
        pass
    return urls


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input', metavar='FILE', nargs='?',
        help='input file', type=argparse.FileType('r'), default=sys.stdin)

    args = parser.parse_args()

    urlweights = read_jl(args.input)
    urlweights.sort(key=lambda x:x[1], reverse=True)
    print_html(urlweights)

if __name__ == '__main__':
    main()

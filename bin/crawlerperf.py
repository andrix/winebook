#!/usr/bin/env python
import sys
import argparse
from urlparse import urlparse

from collections import defaultdict

from winebokbot.utils.jl import JSONLinesReader

def show_domain_distribution(items):
    total = 0
    domains_dist = defaultdict(int)
    try:
        for item in items:
            total += 1
            p = urlparse(item['url'])
            domains_dist[p.hostname] += 1
    except:
        pass

    print "<h2>Data</h2>"
    print "<ul>"
    print "<li> # of pages crawled = %d</li>" % total
    print "</ul>"
    print '<table border="1" cellspacing="0" width="100%">'
    print "<tr><td><strong>Domain</strong></td><td><strong>Page count within the domain</strong></td></tr>"
    maxvalue = float(max(domains_dist.values()))
    for domain, count in sorted(domains_dist.iteritems(), key=lambda x:x[1], reverse=True):
        print '<tr><td>%s</td><td><div style="width: %s%%; background-color:green">%s</div></td></tr>' % (domain, round(count/maxvalue*100, 0), count)
    print "</table>"

def main():
    parser = argparse.ArgumentParser(description="Generate an html file with metrics about the crawler perf")
    parser.add_argument('input', metavar='FILE', nargs='?',
        help='jsonlist with the items (pages scraped)', type=argparse.FileType('r'), default=sys.stdin)
    parser.add_argument('stats', metavar='FILE', nargs='?',
            help='a json file with the stats', type=argparse.FileType('r'), default=None)

    args = parser.parse_args()

    print "<html>"
    print "<body>"
    show_domain_distribution(JSONLinesReader(args.input))
    print "</body>"
    print "</html>"




if __name__ == '__main__':
    main()

"""
Pick a random sample of N documents from the corpus
"""
import sys
import argparse
import random

from winebokbot.utils.jl import JSONLinesReader

def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input', metavar='INPUT CORPUS', nargs=1,
            help='Corpus file in jsonlist format. "url" and "text" keys are required in each json.', 
            type=argparse.FileType('r'),
            default=sys.stdin)
    parser.add_argument('-N', '--sample-size', help="size of the sample",
            default=50, type=int)

    args = parser.parse_args()
    urls = []
    with args.input[0] as fp:
        urls = [(item['url'], item['score']) for item in JSONLinesReader(fp)]
        print "score, url"
        for _ in xrange(args.sample_size):
            rurl, score = random.choice(urls)
            urls.remove((rurl, score))
            print "%s, %s" % (score, rurl)

if __name__ == "__main__":
    main()

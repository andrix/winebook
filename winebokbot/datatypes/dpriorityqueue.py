from urlparse import urlparse
from collections import OrderedDict

from winebokbot import settings

class DPriorityQueueException(Exception):
    pass

class DomainSkipped(Exception):
    pass

class DomainDict(object):
    """
    Dictionary (domain -> list of requests)
    """

    def __init__(self):
        self.domains = OrderedDict()
        self.pos = 0
        self._length = 0
        self._max_urls_per_domain = settings.DQUEUE_URLS_PER_DOMAIN

    def push(self, request):
        parsed = urlparse(request.url)
        if not parsed:
            raise DPriorityQueueException("Error while parsing url: %s" % request.url)
        if parsed.hostname not in self.domains:
            self.domains[parsed.hostname] = []
        if self._max_urls_per_domain and len(self.domains[parsed.hostname]) > self._max_urls_per_domain:
            raise DomainSkipped

        self._length += 1
        self.domains[parsed.hostname].append(request)

    def pop(self):
        if not self.domains:
            raise KeyError("DomainDict is empty")
        domain, requests = self.domains.items()[self.pos]
        request = requests.pop()
        if not requests:
            del self.domains[domain]
        if self.domains:
            self.pos = (self.pos + 1) % len(self.domains)
        self._length -= 1
        return request

    def __len__(self):
        """Return the number of requests on the domaindict object"""
        return self._length

class DPriorityQueue(object):

    def __init__(self, size=None):
        self.data = {}
        self.max_priority = None
        self.size = size
        self.length = 0

    def push(self, request):
        if self.size and self.length == self.size:
            self._pop_minprio()
        if request.priority not in self.data:
            if self.max_priority is None or self.max_priority < request.priority:
                self.max_priority = request.priority
            self.data[request.priority] = DomainDict()
        self.data[request.priority].push(request)
        self.length += 1

    def _pop_minprio(self):
        pmin = min(self.data.keys())
        requests = self.data[pmin]
        req = requests.pop()
        self.length -= 1
        if not requests:
            del self.data[pmin]
        return req

    def pop(self):
        if self.length == 0:
            raise KeyError("DPriorityQueue is empty")
        request = self.data[self.max_priority].pop()
        if not self.data[self.max_priority]:
            del self.data[self.max_priority]
            self.max_priority = max(self.data.iterkeys()) if self.data else None
        self.length -= 1
        return request

    def __len__(self):
        return self.length


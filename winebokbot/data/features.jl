{"january": {"values": ["##NUMBER##"], "weight": 5.0}}
{"february": {"values": ["##NUMBER##"], "weight": 4.82}}
{"april": {"values": ["##NUMBER##"], "weight": 5.0}}
{"june": {"values": ["##NUMBER##"], "weight": 5.0}}
{"july": {"values": ["##NUMBER##"], "weight": 4.96}}
{"august": {"values": ["##NUMBER##"], "weight": 5.01}}
{"october": {"values": ["##NUMBER##"], "weight": 4.94}}
{"november": {"values": ["##NUMBER##"], "weight": 4.94}}
{"acidity": {"values": ["degrees", "zesty", "fruit", "balanced", "vintage", "lively", "balance", "titratable", "higher", "impact", "great", "good", "fresh", "high"], "weight": 5.35}}
{"aging": {"values": ["barrel", "bottle", "prior", "fermentation", "alcohol", "barrels"], "weight": 6.58}}
{"aromas": {"values": ["jasmine", "color", "intense", "fruit", "violet", "cherry", "wine", "smell", "including", "vibrant", "defined", "heightened", "blackberry", "spicy", "lift", "pepper", "floral", "raspberry", "plum", "headier", "with", "glass", "brooding", "flavors", "concentrated"], "weight": 5.0}}
{"barrels": {"values": ["french", "american"], "weight": 3.98}}
{"capacity": {"values": ["height", "guests"], "weight": 10.82}}
{"category": {"values": ["select", "wines", "wine", "shop", "reds", "store", "contact", "color"], "weight": 3.38}}
{"character": {"values": ["cartoon", "varietal", "fruit", "spice", "collection", "lively", "black", "brooding", "smoke", "vanilla", "blackberry", "soft", "grainy", "distinctive", "zinfandel", "advertising"], "weight": 11.76}}
{"characteristics": {"values": ["fruit", "spice", "varietal", "unique", "prime", "zinfandel", "full", "raisiny"], "weight": 6.25}}
{"color": {"values": ["ruby", "purple", "violet", "greater", "yellow", "background", "dark", "garnet", "malolactic", "wine", "inky", "black", "flavor"], "weight": 4.57}}
{"enjoyable": {"values": ["nice", "everyday", "wine", "easy"], "weight": 4.92}}
{"flavor": {"values": ["elegant", "heavy", "cherry", "sugar", "depth", "full", "layers", "brambleberry", "fruit", "berry", "blackberry", "cherry", "cassis", "chocolate", "spicy", "fruity", "spice", "palate", "blueberry", "rich", "ripe", "mouth", "pure"], "weight": 12.33}}
{"reserve": {"values": ["cellars", "sauvignon", "shiraz", "heywood", "##NUMBER##", "noir", "vintners", "zinfandel", "private", "merlot", "syrah"], "weight": 3.71}}
{"style": {"values": ["unknown", "contemporary", "port", "bordeaux", "personal", "teriyaki", "imaginable", "asian", "wine", "older"], "weight": 3.89}}
{"valley": {"values": ["walla", "columbia", "barossa", "clare", "yarra", "hunter", "yakima", "willamette", "alexander", "river", "sonoma", "ynez", "anderson", "creek", "edna", "knights", "maria", "redwood"], "weight": 5.32}}
{"years": {"values": ["last", "this", "first", "thirty", "next", "recent", "previous", "many", "least", "several", "twenty", "three"], "weight": 5.01}}
{"grape_type": {"values": ["merlot", "tannat", "malbec", "cabernet franc", "cabernet sauvignon", "chardonnay", "zinfandel", "riesling", "tempranillo", "syrah", "shiraz", "pinto noir", "petit verdot"], "weight": 12.0}}

import csv
import json

from cStringIO import StringIO
from scrapy import log
from scrapy.spider import BaseSpider
from scrapy.http import Request, HtmlResponse

from winebokbot import settings
from winebokbot.items import EvalItem
from winebokbot.cleaning import TextCleaner
from winebokbot.similarity import SimCompute

from winebokbot.utils.jl import JSONLinesReader

class EvalsimbotSpider(BaseSpider):
    name = "evalsimbot"
    allowed_domains = []
    start_urls = []

    def __init__(self, *args, **kwargs):
        super(EvalsimbotSpider, self).__init__(*args, **kwargs)
        seeds_url = kwargs.get('urls')
        url = kwargs.get('url')
        if url:
            self.start_urls = [url]
        else:
            self.start_urls = [seeds_url] if seeds_url else []

        jlw = JSONLinesReader(open("winebokbot/data/weighted_corpus.jl"))
        idfdb = json.load(open("winebokbot/data/idf_db.json"))

        self.scorer = SimCompute(list(jlw), idfdb)
        self.html_cleaner = TextCleaner(settings.CLEAN_METHOD)

    def parse(self, response):
        # start_urls is filled by the seeds_url paramter that should contains
        # a list of url each per line to start crawling .
        for row in csv.reader(StringIO(response.body)):
            yield Request(row[0], callback=self.parse_target, meta={'url': row[0]})

    def parse_target(self, response):
        if not isinstance(response, HtmlResponse):
            return
        item = EvalItem()
        item['url'] = response.meta['url']
        try:
            text = response.body_as_unicode()
            text = self.html_cleaner.clean(text.encode('utf8'))
            item['score'] = self.scorer.similarity(text)
        except Exception, e:
            self.log("Error when cleaning html. url = %s. Exception: %s" % (item['url'], str(e)), log.ERROR)
            item['score'] = 0.0
        yield item



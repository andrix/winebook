import csv
import json

from cStringIO import StringIO
from scrapy.spider import BaseSpider
from scrapy.http import Request, HtmlResponse
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.lxmlhtml import LxmlParserLinkExtractor
from scrapy import log

from winebokbot import settings
from winebokbot.items import PageItem
from winebokbot.similarity import SimCompute
from winebokbot.utils.jl import JSONLinesReader

from winebokbot.cleaning import TextCleaner

class WinebotSpider(BaseSpider):
    name = "winebot"
    allowed_domains = []
    start_urls = []

    def __init__(self, *args, **kwargs):
        super(WinebotSpider, self).__init__(*args, **kwargs)
        seeds_url = kwargs.get('seeds_url')
        url = kwargs.get('url')
        if url:
            self.start_urls = [url]
        else:
            self.start_urls = [seeds_url] if seeds_url else []

        jlw = JSONLinesReader(open("winebokbot/data/weighted_corpus.jl"))
        idfdb = json.load(open("winebokbot/data/idf_db.json"))

        self.scorer = SimCompute(list(jlw), idfdb)
        self.linkextractor = LxmlParserLinkExtractor(unique=True)
        self.html_cleaner = TextCleaner(settings.CLEAN_METHOD)

    def parse(self, response):
        # start_urls is filled by the seeds_url paramter that should contains
        # a list of url each per line to start crawling .
        for row in csv.reader(StringIO(response.body)):
            yield Request(row[0], callback=self.parse_target, priority=15)

    def parse_target(self, response):
        if not isinstance(response, HtmlResponse):
            return

        hxs = HtmlXPathSelector(response)
        item = PageItem()
        item['url'] = response.url
        title = hxs.select("//head/title/text()").extract()
        if title:
            item['page_title'] = title[0]

        content = response.body_as_unicode()
        item['page_content'] = content
        text = self.html_cleaner.clean(content.encode('utf8'))
        score = self.scorer.similarity(text)
        item['score'] = score
        #if score < 0.51:
        #    stats.inc_value('irrelevant_pages')
        #    return
        yield item

        try:
            links = self.linkextractor.extract_links(response)
        except Exception, e:
            self.log("LinkExtractor failed to extract links from url = %s. Exception: %s" % (response.url, str(e)), log.ERROR)
            return

        priority = int(round(score*10, 0))
        for link in links:
            yield Request(link.url, priority=priority, callback=self.parse_target)




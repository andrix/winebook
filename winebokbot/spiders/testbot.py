from scrapy.spider import BaseSpider
from scrapy.http import Request


class WinebotSpider(BaseSpider):
    name = "testbot"
    allowed_domains = []

    def start_requests(self):
        # start_urls is filled by the seeds_url paramter that should contains
        # a list of url each per line to start crawling .

        yield Request("http://www.elpais.com.uy")

    def parse(self, response):
        requests = [
            Request("http://www.google.com", priority=1, callback=self.parse_target),
            Request("http://scrapinghub.com", priority=2, callback=self.parse_target),
            Request("http://www.facebook.com", priority=3, callback=self.parse_target),
            Request("http://www.change.org", priority=4, callback=self.parse_target),
            Request("http://www.fing.edu.uy", priority=5, callback=self.parse_target),
        ]

        for req in requests:
            yield req

    def parse_target(self, response):
        print "URL: %s" % response.url


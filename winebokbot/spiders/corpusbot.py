import csv

from cStringIO import StringIO
from scrapy import log
from scrapy.spider import BaseSpider
from scrapy.http import Request, HtmlResponse

from winebokbot import settings
from winebokbot.items import CorpusItem
from winebokbot.cleaning import TextCleaner

class CorpusbotSpider(BaseSpider):
    name = "corpusbot"
    allowed_domains = []
    start_urls = []

    def __init__(self, *args, **kwargs):
        super(CorpusbotSpider, self).__init__(*args, **kwargs)
        seeds_url = kwargs.get('urls')
        url = kwargs.get('url')
        if url:
            self.start_urls = [url]
        else:
            self.start_urls = [seeds_url] if seeds_url else []
        self.html_cleaner = TextCleaner(settings.CLEAN_METHOD)

    def parse(self, response):
        # start_urls is filled by the seeds_url paramter that should contains
        # a list of url each per line to start crawling .
        docid = 1
        for row in csv.reader(StringIO(response.body)):
            yield Request(row[0], callback=self.parse_target, meta={'url': row[0], 'docid': docid})
            docid += 1

    def parse_target(self, response):
        if not isinstance(response, HtmlResponse):
            return

        item = CorpusItem()
        item['url'] = response.meta['url']
        item['docid'] = response.meta['docid']
        try:
            text = response.body_as_unicode()
            item['text'] = self.html_cleaner.clean(text.encode('utf8'))
        except Exception, e:
            self.log("Error when cleaning html. url = %s. Exception: %s" % (item['url'], str(e)), log.ERROR)

        yield item



"""
Implement similarity functions based on the article of Meczner et al.
"""
import heapq

from winebokbot.ir.distances import cosinevector
from winebokbot.ir.weights import termfreq_vector, TFIDFScoring

class SimCompute(object):
    """
    Compute the similarity between the corpus and a page.

    constructor:
        * corups - a list with dictionaries
        * idf - a dictionary with (term, idf) weights
    """

    def __init__(self, corpus, idf):
        self.corpus = corpus
        self.scorer = TFIDFScoring(len(corpus), idfvector=idf)

    def _similarity(self, text):
        """
        Compute the similarity of the page to the current corpus
        """

        termfreq_vec = termfreq_vector(text)
        vecpage = self.scorer.compute(termfreq_vec)

        result = []
        for doc in self.corpus:
            vec = dict(doc['weights'])
            heapq.heappush(result, (cosinevector(vec, vecpage)*-1, doc['docid']))
        return result

    def similarity(self, text):
        """
        text - just the text to compute similarity
        """
        sim_vector = self._similarity(text)
        sim, docid = heapq.heappop(sim_vector)
        return sim*-1
        




from scrapy.utils.misc import load_object

from scrapy.core.scheduler import Scheduler

from winebokbot.datatypes import DPriorityQueue, DomainSkipped

class FairPageScheduler(Scheduler):

    def __init__(self, dupefilter, stats=None):
        self.df = dupefilter
        self.stats = stats
        self.dpq = None

    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings
        dupefilter_cls = load_object(settings['DUPEFILTER_CLASS'])
        dupefilter = dupefilter_cls.from_settings(settings)
        cls.queue_size = settings['DQUEUE_SIZE']
        return cls(dupefilter, stats=crawler.stats)

    def has_pending_requests(self):
        return len(self) > 0

    def open(self, spider):
        self.spider = spider
        if self.dpq:
            del self.dpq
        self.dpq = DPriorityQueue(size=self.queue_size)
        self.stats.set_value('scheduler/queue/size', self.dpq.size,
            spider=self.spider)

    def close(self, reason):
        del self.dpq
        return self.df.close(reason)

    def enqueue_request(self, request):
        if not request.dont_filter and self.df.request_seen(request):
            return
        try:
            self.dpq.push(request)
            self.stats.inc_value('scheduler/queue/enqueued_by_prio/%d' % request.priority,
                spider=self.spider)
            self.stats.set_value('scheduler/queue/count', self.dpq.length,
                spider=self.spider)
        except DomainSkipped:
            self.stats.inc_value('scheduler/domain_skipped', spider=self.spider)

    def next_request(self):
        if not self.dpq:
            return
        request = self.dpq.pop()
        if request:
            self.stats.inc_value('scheduler/queue/dequeued_by_prio/%d' % request.priority,
                    spider=self.spider)
        return request

    def __len__(self):
        return len(self.dpq)

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class PageItem(Item):
    # define the fields for your item here like:
    # name = Field()
    url = Field()
    page_title = Field()
    page_content = Field()
    score = Field()

    def __str__(self):
        content = self['page_content'].lstrip()[:50]\
                .replace('\n', '')\
                .replace('\r', '')
        return 'Page(url="%s", title="%s", score=%.2f, content="%s")' % (self['url'], self['page_title'], self['score'], content)

    def __repr__(self):
        return str(self)

class CorpusItem(Item):
    url = Field()
    text = Field()
    docid = Field()


class EvalItem(Item):
    url = Field()
    score = Field()


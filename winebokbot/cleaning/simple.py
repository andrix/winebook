"""
Simple functions to clean html
"""
from w3lib.html import remove_comments, remove_tags, remove_tags_with_content

def clean_html(htmlpage):
    """Return just the text with all the tags removed"""

    h = remove_comments(htmlpage)
    h = remove_tags_with_content(h, which_ones=('head', 'script'))
    return remove_tags(h)



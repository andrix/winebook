from .simple import clean_html
from .textify import textify

class TextCleanerException(Exception):
    pass

class TextCleaner(object):

    def __init__(self, method):
        self.method = method
        if method == 'simple':
            self._clean = clean_html
        elif method == 'textify':
            self._clean = textify
        else:
            raise TextCleanerException("method '%s' is not implemented yet" % method)

    def clean(self, page):
        return self._clean(page)

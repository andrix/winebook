import lxml.html

def _textify(html):
    root = lxml.html.fromstring(html)
    pieces = []
    total = 0.0
    nelems = 0
    for elem in root.getiterator():
        if elem.tag in ('html', 'head', 'script', 'style') or elem.tag is lxml.etree.Comment:
            continue
        n = len(list(elem.iterchildren()))
        text = elem.text
        if not text:
            continue
        ntext = len(text.split())
        if ntext == 0:
            continue
        value = float(n)/ntext
        nelems += 1
        total += value
        pieces.append((value, elem.tag, text))
    avg = total/nelems
    return sorted(filter(lambda x: x[0] < avg, pieces), reverse=True)


def textify(html):
    return " ".join(t for _, _, t in _textify(html))

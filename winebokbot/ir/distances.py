import math

def dotproduct(vec1, vec2):
    return sum(x * y for x, y in zip(vec1, vec2))

def norm(vec):
    return math.sqrt(sum(vi**2 for vi in vec))

def cosinevector(vec1, vec2):
    """
    Compute the cosine vector between two vectors.

    vec1, vec2 - a dictionary with key = term and value = weight

    cosinvec =       V1 . V2         # dot product
              -------------------
              || V1 || * || V2 ||

    >>> vec1 = {'a': 10, 'b': 5, 'c': 6}
    >>> vec2 = {'a': 10, 'b': 5, 'c': 6}
    >>> cosinevector(vec1, vec2)
    1.0
    >>> vec1 = {'a': 10, 'c': 6}
    >>> vec2 = {'a': 10, 'b': 5, 'c': 6}
    >>> cosinevector(vec1, vec2) < 1
    True
    >>> cosinevector(vec1, {})
    0.0
    >>> vec1 = {'a1': 10, 'c2': 6}
    >>> vec2 = {'a': 10, 'b': 5, 'c': 6}
    >>> cosinevector(vec1, vec2) 
    0.0
    >>>
    """

    commonkeys = sorted(set(vec1.keys()) & set(vec2.keys()))
    v1 = map(lambda key: vec1[key], commonkeys)
    v2 = map(lambda key: vec2[key], commonkeys)

    try:
        return dotproduct(v1, v2) / (norm(vec1.values()) * norm(vec2.values()))
    except ZeroDivisionError:
        return 0.0



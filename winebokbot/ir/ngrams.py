"""
N-grams utilities
"""

def compute_ngram(tokens, n=2):
    """
    >>> l = ['a', 'b', 'c', 'd']
    >>> g =compute_ngram(l)
    >>> g.next()
    ('a', 'b')
    >>> g.next()
    ('b', 'c')
    >>> g.next()
    ('c', 'd')
    >>> list(g)
    []
    >>> g = compute_ngram(l, n=3)
    >>> g.next()
    ('a', 'b', 'c')
    >>> g.next()
    ('b', 'c', 'd')
    >>> list(g)
    []
    >>>
    """
    for i in xrange(len(tokens) - (n-1)):
        yield tuple(tokens[i:i+n])


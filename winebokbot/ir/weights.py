import math
from collections import defaultdict

from winebokbot.utils.tokenizer import tokenize_stem, STOPWORDS


def termfreq_vector(text, min_length=3, stopwords=STOPWORDS):
    """Returns a dictionary with ``term`` as key, and the term frequency computed
    from the ``text`` argument as value"""

    weights = defaultdict(int)
    for term in tokenize_stem(text, min_length=min_length, stopwords=stopwords):
        weights[term] += 1
    return weights

class ScoringBase(object):

    def compute(self):
        pass


class TFIDFScoring(ScoringBase):
    """
    TFIDFScoring

    Args:
        N - number of documents in the collection
        docfreq - dictionary with key=term and value=# of docs that contains `term`
        idfvector - already computed idfvector
        smart_schema - 
         refer to: http://nlp.stanford.edu/IR-book/html/htmledition/document-and-query-weighting-schemes-1.html
         current implemented options:
             * lnc - use for documents
             * ltc - use for queries
    """

    def __init__(self, N, docfreq=None, idfvector=None, smart_schema='ltc'):
        self.docfreq = docfreq
        self.collectionsize = N
        self._idfvector = idfvector
        self.smart_schema = smart_schema
        termfrequency = {
            'l': self._wf,
            'n': lambda tf: tf, # assuming the argument is the TF itself
        }
        documentfrequency = {
            't': lambda term: self.idfvector.get(term, 0),
            'n': lambda term: 1
        }
        normalization = {
            'c': self._normalization,
            'n': lambda _: 1,
        }
        self.tf_func = termfrequency.get(smart_schema[0])
        self.df_func = documentfrequency.get(smart_schema[1])
        self.nf_func = normalization.get(smart_schema[2])
        self.tfidf = lambda term, tf: self.tf_func(tf) * self.df_func(term)

    def _idf(self, df):
        return math.log(self.collectionsize / float(df))

    def _wf(self, tf):
        return 1 + math.log(tf)

    def _normalization(self, weights):
        v = math.sqrt(sum(w**2 for _, w in weights.iteritems()))
        return 1/v if v else 0.0

    @property
    def idfvector(self):
        if not self._idfvector:
            idf = self._idf
            self._idfvector = dict((term, idf(df))
                for term, df in self.docfreq.iteritems())
        return self._idfvector

    def compute(self, termfreqvector):
        it = termfreqvector.iteritems() if isinstance(termfreqvector, dict) else termfreqvector

        weights = {}
        tfidf = self.tfidf
        for term, tf in it:
            weights[term] = round(tfidf(term, tf), 3)

        normfactor = self.nf_func(weights)
        return dict((term, w*normfactor) for term, w in weights.iteritems())



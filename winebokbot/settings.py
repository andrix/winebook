# Scrapy settings for winebokbot project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'winebokbot'

SPIDER_MODULES = ['winebokbot.spiders']
NEWSPIDER_MODULE = 'winebokbot.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'winebokbot (+http://www.yourdomain.com)'

EXTENSIONS = {
    'scrapy.contrib.closespider.CloseSpider': 500,
}

DOWNLOADER_MIDDLEWARES = {
    'winebokbot.middlewares.CheckPriorityOrder': None,
}

RETRY_ENABLED = False
REDIRECT_PRIORITY_ADJUST = 0

SPIDER_MIDDLEWARES = {
    'scrapy.contrib.spidermiddleware.depth.DepthMiddleware': None,
}

SMART_SCHEME_DOC = 'lnc'
SMART_SCHEME_PAGES = 'ltc'
CLEAN_METHOD = 'textify'

CLOSESPIDER_ITEMCOUNT = 2000

SCHEDULER = 'winebokbot.fairpagescheduler.FairPageScheduler'

DQUEUE_URLS_PER_DOMAIN = 20
DQUEUE_SIZE = 256

import nltk
import string

# setup tokenize and stem functions - as they're simple enough it's
# OK to just leave as below

# tokenize = nltk.tokenize.wordpunct_tokenize
_tokenize = nltk.tokenize.RegexpTokenizer("[A-Za-z]+").tokenize
_stem = nltk.stem.porter.PorterStemmer().stem

# setup stopwords from NLTK
STOPWORDS = nltk.corpus.stopwords.words('english')

def tokenize(text, min_length=3, stopwords=STOPWORDS, tokenize_func=_tokenize):
    """Returns a dictionary with ``term`` as key, and the term frequency computed
    from the ``text`` argument as value"""

    for token in tokenize_func(text):
        if len(token) <= min_length or token in stopwords \
            or token in string.punctuation:
            continue
        yield token.lower()

def tokenize_stem(text, min_length=3, stopwords=STOPWORDS):
    """Returns a dictionary with ``term`` as key, and the term frequency computed
    from the ``text`` argument as value"""

    return (_stem(token) for token in tokenize(text, min_length=min_length,
        stopwords=stopwords))


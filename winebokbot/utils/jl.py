import json

class JSONLinesBase(object):
    """Base class to implement JSONLines utils"""

    def __init__(self, file_like_object):
        self.fp = file_like_object

    def serialize(self, obj, **kwargs):
        return json.dumps(obj)

    def deserialize(self, strobj, **kwargs):
        return json.loads(strobj)


class JSONLinesReader(JSONLinesBase):
    r"""Reader class for JSONlines files

    >>> from cStringIO import StringIO
    >>> s1, s2 = json.dumps({'key1': 'v1'}), json.dumps({'key2': 'v2'})
    >>> fo = StringIO("%s\n%s\n" % (s1, s2))
    >>> jlr = JSONLinesReader(fo)
    >>> jlr.read_next()
    {u'key1': u'v1'}
    >>> jlr.read_next()
    {u'key2': u'v2'}
    >>> jlr.read_next() is None
    True
    >>> fo.seek(0)
    >>> l1 = list(k for obj in jlr.read() for k in obj.keys())
    >>> 'key1' in l1 and 'key2' in l1
    True
    >>> fo.seek(0)
    >>> l2 = list(k for obj in jlr for k in obj.keys())
    >>> 'key1' in l2 and 'key2' in l2
    True
    >>> 

    """

    def read_next(self):
        line = self.fp.readline().strip()
        return self.deserialize(line) if line else None

    def read(self):
        for line in self.fp:
            yield self.deserialize(line.strip())

    def __iter__(self):
        return self.read()

class JSONLinesWriter(JSONLinesBase):
    r"""Writer class for JSONLines files

    >>> from cStringIO import StringIO
    >>> fo = StringIO()
    >>> jlw = JSONLinesWriter(fo)
    >>> jlw.write({'key1': 'v1'})
    >>> jlw.write({'key2': 'v2'})
    >>> fo.getvalue()
    '{"key1": "v1"}\n{"key2": "v2"}\n'
    >>>
    >>> fo2 = StringIO()
    >>> jlw = JSONLinesWriter(fo2)
    >>> jlw.write_iter([{'key1': 'v1'}, {'key2': 2}])
    >>> fo2.getvalue()
    '{"key1": "v1"}\n{"key2": 2}\n'
    >>>


    """

    def write(self, obj):
        self.fp.write("%s\n" % self.serialize(obj))

    def write_iter(self, iter):
        for elem in iter:
            self.write(elem)

